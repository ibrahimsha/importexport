from server import create_app, db

app = create_app()

with app.app_context():
    db.Model.metadata.reflect(db.engine)

class Fields(db.Model):
    """
    Fields database table model class
    """
    __table__ = db.Model.metadata.tables["fields"]

    def __repr__(self):
        return "<Field {}>".format(self.fieldName)

class Modules(db.Model):
    """
    Modules database table model class
    """
    __table__ = db.Model.metadata.tables["modules"]

    def __repr__(self):
        return "<Module {}>".format(self.name)

class Course(db.Model):
    """
    Course Section Master database table model class
    """
    __table__ = db.Model.metadata.tables["mst_course"]

    def __repr__(self):
        return "<Course {}>".format(self.name)

class CourseSection(db.Model):
    """
    Course Section Master database table model class
    """
    __table__ = db.Model.metadata.tables["mst_course_section"]

    def __repr__(self):
        return "<Course Section {}>".format(self.name)

class ExamName(db.Model):
    """
    Exam Name Master database table model class
    """
    __table__ = db.Model.metadata.tables["mst_exam_name"]

    def __repr__(self):
        return "<Exam name {}>".format(self.name)

class Program(db.Model):
    """
    Program database model class
    """
    __table__ = db.Model.metadata.tables["mst_program"]

    def __repr__(self):
        return "<Program {}>".format(self.name)

class QuestionBank(db.Model):
    """
    Question bank database model class
    """
    __table__ = db.Model.metadata.tables["mst_question_bank"]

    def __repr__(self):
        return "<Question {}>".format(self.name)

class StatusRule(db.Model):
    """
    Status Rule database model class
    """
    __table__ = db.Model.metadata.tables["map_status_rule"]

    def __repr__(self):
        return "<Status rule {}>".format(self.name)

class NbaQuestionTemplate(db.Model):
    """
    Nba Question Template database table model class
    """
    __table__ = db.Model.metadata.tables["mst_nba_question_template"]

    def __repr__(self):
        return "<Question template {}>".format(self.name)
