from flask import Response, request, jsonify, g
from server import logger

def send_response(data=[], errors=[], status=200):
    """
    Global function to format the api response
    """
    return jsonify({"data": data, "errors": errors}), status

def make_log(message="", level="INFO", module_name=None):
    """
    Prepare and render the log message
    """
    if level=="INFO":
        logger.info(message, extra={"modulename": module_name})
    elif level=="ERROR":
        logger.error(message, extra={"modulename": module_name})
    else:
        logger.warning(message, extra={"modulename": module_name})

def log_before_request():
    """
    Log the request before proceeding to process
    """
    message = "{}:{} called from {}".format(
        request.method,
        request.url,
        request.remote_addr
    )
    return make_log(message)

def bad_request(e):
    """
    Function to handle the 400 response status
    """
    return send_response(errors={"message": "Invalid input"}, status=400)

def page_not_found(e):
    """
    Function to handle the 404 response status
    """
    return send_response(errors={"message": "Page you are looking for not found"}, status=404)

def internal_server_error(e):
    """
    Function to handle the internal server error
    """
    return send_response(errors={"error": "Something went wrong, please try again"}, status=500)

def validate(type):
    """
    Function to send the validation for excel type
    """

    # For integer type
    if (type == 'int'):
        return {'validate': 'integer',
                'criteria': '>',
                'value': 0,
                'input_title': 'Enter an integer:',
                'error_title': 'Input value is not valid!',
                'error_message': 'It should be an integer and greater than zero'}
 
    # For decimal Validation
    elif type == 'float':
        return {'validate': 'decimal',
                'criteria': 'between',
                'minimum':0.1,
                'maximum': 255,
                'value': 0,
                'input_title': 'Enter some characters integer:',
                'error_title': 'Input value is not valid!',
                'error_message': 'It should be an string and need more than zero characters'}

    # For Srting type
    elif (type == 'varchar') or (type == 'text'):
        return {'validate': 'integer',
                'criteria': 'between',
                'minimum':0,
                'maximum': 255,
                'value': 0,
                'input_title': 'Enter some characters integer:',
                'error_title': 'Input value is not valid!',
                'error_message': 'It should be an string and need more than zero characters'} 
    else:
        return type