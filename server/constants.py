from server.models import *

constants = {
    "questionpaper": {
        "models": ["questionpaper",]
    },
    "datatables": {
        "fields": Fields,
        "modules": Modules,
        "mst_course": Course,
        "mst_course_section": CourseSection,
        "mst_exam_name": ExamName,
        "mst_program": Program,
        "mst_question_bank": QuestionBank,
        "map_status_rule": StatusRule,
        "mst_nba_question_template": NbaQuestionTemplate
    }
}