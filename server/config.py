import os

class Config(object):
    """
    Global configuration object
    """
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://{}:{}@{}/{}".format(
        os.environ.get("DB_USER"),
        os.environ.get("DB_PASSWORD"),
        os.environ.get("DB_HOST"),
        os.environ.get("DB_NAME")
    )
    SQLALCHEMY_TRACK_MODIFICATIONS=False

class LoggerConfig(object):
    """
    Global log configuration
    """
    dictConfig = {
        "version": 1,
        "formatters": {
            "standard": {
                "format": "%(asctime)s - %(name)s - %(message)s - %(modulename)s"
            },
            "minimal": {
                "format": "%(asctime)s - %(message)s - %(modulename)s"
            }
        },
        "handlers": {
            "wsgi": {
                "level": "DEBUG",
                "class": "logging.FileHandler",
                "filename": "logs/eximservice.log",
                "formatter": "standard"
            },
        },
        "root": {
            "handlers": ["wsgi"],
            "level": "INFO"
        }
    }
