import logging, logging.config
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from server.config import Config, LoggerConfig

logging.config.dictConfig(LoggerConfig.dictConfig) # Global logger config initialization
logger = logging.getLogger("root")

db = SQLAlchemy() # Initializing the database object

def create_app():
    """
    Function to initialize the app object
    """
    app = Flask(__name__)

    # Register global configuraiton to the app object
    app.config.from_object(Config)

    # Registering the database object
    db.init_app(app)

    # import the helper functions
    from server import helpers

    # Register the signal events for app object
    app.before_request(helpers.log_before_request)

    # Register the global error handlers
    app.register_error_handler(404, helpers.page_not_found)
    app.register_error_handler(400, helpers.bad_request)
    app.register_error_handler(500, helpers.internal_server_error)

    # import and register the blueprints to app object
    from questionpaper import qpaper
    app.register_blueprint(qpaper)

    return app