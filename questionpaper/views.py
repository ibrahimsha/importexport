import os
from collections import OrderedDict
from flask import send_file, jsonify
from questionpaper import qpaper
from server.helpers import send_response, make_log
from server.models import Fields, Modules
from server.constants import constants
from lib.excelrender import ExcelRender
from phpserialize import serialize, unserialize

@qpaper.route("/api/exim/template")
def index():
    """
    Function to export template for question paper
    """
    try:
        make_log("Retrieve logs for questionpaper module")
        models = constants["questionpaper"]["models"]
        for model in models:
            field_rows = Fields.query.filter_by(modelName=model).all()
            er = ExcelRender(engine='xlsxwriter')
            er.add_worksheet(sheet_name="Master")
            for row in field_rows:
                if row.type == "link":
                    linkVariables = get_link_variables(row.linkType, row.field_group)
            er.populate_worksheet([row.attributeLabel for row in field_rows])
            workbook_path = er.get_workbook()
            make_log("Return the rendered excel file ", workbook_path)
            return send_file(filename_or_fp=workbook_path)
    except Exception as e:
        make_log(str(e))


def get_link_variables(linkType, field_group):
    """
    Function to render the lookup values
    for the excel column
    """
    byte_object = unserialize(str.encode(field_group), array_hook=dict)
    py_objects = byte2str(byte_object)
    link_record = Modules.query.filter_by(name=linkType).first()
    if link_record:
        result_values = constants["datatables"][link_record.table_name].query.all()
        result_json = [record.__dict__ for record in result_values]
        lookup_values = [res[py_objects["name"]] for res in result_json]
        return lookup_values
    return None

def byte2str(data):
    if isinstance(data, bytes):  return data.decode('ascii')
    if isinstance(data, dict):   return dict(map(byte2str, data.items()))
    if isinstance(data, tuple):  return map(byte2str, data)
    return data