from dotenv import load_dotenv
from pathlib import Path

envpath = Path(".") / ".env"
load_dotenv(dotenv_path=envpath)

from server import create_app
app = create_app()