FROM python:3.6
LABEL MAINTAINER "<dilip.dakshinapu@anubavam.com>D R DILIP"

RUN mkdir -p /usr/src/app/exim
WORKDIR /usr/src/app/exim

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . ./

RUN ["chmod", "+x", "boot.sh"]

ENTRYPOINT ["./boot.sh"]