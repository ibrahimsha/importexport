#!/bin/bash
gunicorn run:app -b 0.0.0.0:8000 -w 4 --reload --error-logfile logs/gunicorn.log