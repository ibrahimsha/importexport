import os
import uuid
import pandas as pd
from server.helpers import make_log


class ExcelRender:
    """
    Excelrender function takes input as filename
    and the required information to generate the 
    excel file in dynamic
    """

    def __init__(self, engine):
        self.filename = uuid.uuid4().hex[:10].upper() + ".xlsx"
        self.filepath = os.path.abspath("media")
        self._writer = pd.ExcelWriter(os.path.join(self.filepath, self.filename), engine=engine)
        self._workbook = self._writer.book
        self.header_format = self._workbook.add_format({
                                'border': 1,
                                'bg_color': '#C6EFCE',
                                'bold': True,
                                'text_wrap': True,
                                'valign': 'vcenter',
                                'indent': 1,
                            })

    def add_worksheet(self, sheet_name):
        """
        Function to generate the worksheet
        """
        self._worksheet = self._workbook.add_worksheet(sheet_name)

    def populate_worksheet(self, columns):
        """
        Function to populate the worksheet with validations
        and headers
        """
        
        for colCount, column in enumerate(columns):
            self._worksheet.write(0, colCount, column, self.header_format)
            self._worksheet.data_validation(1,colCount,1000,colCount,{"validate": "list", "source": ['open', 'high', 'close']})
    
    def get_workbook(self):
        """
        Function to return the rendered excel file
        """        
        try:
            self._workbook.close()
            self._writer.save()
            return os.path.join(self.filepath, self.filename)
        except Exception as e:
            make_log(str(e))

    def clear_media(self):
        """
        Function to clear the media directory
        """
        try:
            for item in self.filepath:
                if item.endswith(".xlsx"):
                    os.remove(item)
        except Exception as e:
            make_log(str(e))

    
